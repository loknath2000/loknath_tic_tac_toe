const boxes = Array.from(document.querySelectorAll('.box'));
const playerDisplay = document.querySelector('.player');
const resetButton = document.querySelector('#reset');

let board = ['', '', '', '', '', '', '', '', ''];
let currentPlayer = 'X';
let isGameActive = true;

const PLAYERX_WON = 'PLAYERX_WON';
const PLAYERO_WON = 'PLAYERO_WON';
const TIE = 'TIE';



// storing winning conditions in an variable


const winningConditions = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
];


// function for validating result who won

const handleResultValidation = () => {
    let roundWon = false;
    for (let i = 0; i <= 7; i++) {
        const winCondition = winningConditions[i];
        const a = board[winCondition[0]];
        const b = board[winCondition[1]];
        const c = board[winCondition[2]];
        if (a === '' || b === '' || c === '') {
            continue;
        } else if (a === b && b === c) {
            roundWon = true;
            break;
        }
    }

    if (roundWon === true) {
        announce(currentPlayer === 'X' ? PLAYERX_WON : PLAYERO_WON);
        isGameActive = false;
    } else if (board.includes('') === false) {
        announce(TIE);
    }

}


// announce function to declare winner

const announce = (type) => {
    if (type === PLAYERO_WON) {
        alert("PLAYER O WON")
    } else if (type === PLAYERX_WON) {
        alert("PLAYER X WON")
    } else {
        alert("TIE")
    }
};

// function for checking if inner text is empty or filled

const isValidAction = (box) => {
    if (box.innerText === 'X' || box.innerText === 'O') {
        return false;
    } else {
        return true;
    }
};


// update function to update the currentplayer details in board using index of currentplayer

const updateBoard = (index) => {
    board[index] = currentPlayer;
}

// function to change the player in display along with the game

const changePlayer = () => {
    playerDisplay.classList.remove(`player${currentPlayer}`);
    currentPlayer = currentPlayer === 'X' ? 'O' : 'X';
    playerDisplay.innerText = currentPlayer;
    playerDisplay.classList.add(`player${currentPlayer}`);
}


// iterating over the each element in an function

boxes.forEach((box, index) => {
    box.addEventListener('click', () => {
        if (isValidAction(box) && isGameActive) {
            box.innerText = currentPlayer;
            box.classList.add(`player${currentPlayer}`);
            updateBoard(index);
            handleResultValidation();
            changePlayer();
        }
    });
});

// reseting the game after completing it


resetButton.addEventListener('click', () => {
    board = ['', '', '', '', '', '', '', '', ''];
    isGameActive = true;

    if (currentPlayer === 'O') {
        changePlayer();
    }

    boxes.forEach(box => {
        box.innerText = '';
        box.classList.remove('playerX');
        box.classList.remove('playerO');
    });
});